﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // Public Properties
    [Tooltip("Walking speed of player")]
    public float PlayerSpeed = 5.0f;

    [Tooltip("Checkout Time of player (in seconds)")]
    public float CheckoutTime = 5.0f;

    [Tooltip("Maximum germ level of player")]
    public float MaxGermLevel = 100.0f;

    [Tooltip("Time player has to finish level (in seconds)")]
    public float LevelTime = 65.0f;

    [Tooltip("Low time warning (in seconds)")]
    public float WarningTime = 15.0f;

    [Tooltip("Splat Sound")]
    public AudioClip SplatSound;

    [Tooltip("Time Low Sound")]
    public AudioClip TimeLowSound;

    [Tooltip("Losing sound")]
    public AudioClip LosingSound;

    [Tooltip("Winning sound")]
    public AudioClip WinningSound;

    [Tooltip("Picking up sound")]
    public AudioClip PickupSound;

    [Tooltip("Maximum Parallel Sounds")]
    public int MaxSounds = 10;

    [Tooltip("GameObject to enable when something is in our pickup zone")]
    public GameObject PickUpIndicator;

    [Tooltip("GameObject to enable when we are ready to get to the counter")]
    public GameObject CounterIndicator;

    [Tooltip("GameObject to enable when we are in a checkout zone and ready to checkout")]
    public GameObject CheckoutIndicator;

    public GermoMeter Ui_GermoMeter;
    public StatusBar Ui_CheckoutBar;
    public Text Ui_TimerText;

    // My Objects
    private Rigidbody _myRigidBody;
    private PersonBody _myPersonBody;
    private Collider _myPickupZone;

    // Private state variables
    private float _germLevel = 0.0f;
    private List<Cough> _currentCaughs = new List<Cough>();
    private float _coughTimer = 0.0f;
    private float _levelTimer = 0.0f;
    private bool _pickupPressed = false;
    private float _inputTimer = 0.0f;
    private float _gfxUpdateTimer = 0.0f;
    private float _currentInput_x = 0.0f;
    private float _currentInput_y = 0.0f;
    private AudioSource[] _audioScources;
    private float _checkPickupZoneTimer = 0.0f;
    private ShoppingItem _currentItemInZone = null;
    private bool _finishedShopping = false;
    private bool _checkoutInZone = false;
    private GameObject _checkOutObject = null;
    private float _checkoutTimer = 0.0f;
    private bool _checkingOut = false;
    private float _timerFlashPeriod = 0.2f;
    private float _timerFlashTimer = 0.0f;
    private int _timerFlashFlashTimes = 4;
    private int _timerFlashNo = 0;
    private bool _timerWarned = false;
    private bool _gameFinished = false;


    // Start is called before the first frame update
    void Start()
    {
        _myRigidBody = null;

        _myRigidBody = GetComponent<Rigidbody>();
        if (_myRigidBody == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a Rigidbody!");
        }

        _myPersonBody = GetComponentInChildren<PersonBody>();
        if (_myPersonBody == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a PersonBody!");
        }
        else
        {
            _myPersonBody.GermHitEvent += this.OnGermHit;
        }

        if (Ui_GermoMeter == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a germo-meter assigned!");
        }

        _myPickupZone = getCurrentPickUpZone();
        if(_myPickupZone == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a germo-meter assigned!");
        }

        // setup germ o meter by 'adjusting' it
        adjustGermAmount(0);

        // Set level timer
        setRemainingTime(LevelTime);

        // setup audio sources
        _audioScources = new AudioSource[MaxSounds];
        for(int i = 0; i < MaxSounds; i++)
        {
            _audioScources[i] = gameObject.AddComponent<AudioSource>();
            _audioScources[i].spatialBlend = 1.0f;
        }

        // make sure background music is playing
        SetBackgroundMusicActive(true);

        // disable the counter indicator
        CounterIndicator.SetActive(false);
        CheckoutIndicator.SetActive(false);
        Ui_CheckoutBar.gameObject.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if((_gameFinished == true) || (GameManager.MainGameManager.IsPaused() == true))
        {
            return;
        }

        // Update player graphics not too frequently because sometimes the graphics freaks out
        if (_gfxUpdateTimer > 0.02f)
        {
            _gfxUpdateTimer = 0.0f;
            // Update direction of body and animation
            updatePersonBodyDir(_currentInput_x, _currentInput_y);
        }
        else
        {
            _gfxUpdateTimer += Time.deltaTime;
        }


        // check input for movement
        if (_inputTimer > 0.1f)
        {
            _inputTimer = 0.0f;

            // Get input and set speed
            _currentInput_x = Input.GetAxisRaw("Horizontal");
            _currentInput_y = Input.GetAxisRaw("Vertical");

            Vector3 newSpeed = new Vector3(_currentInput_x, 0.0f, _currentInput_y); // Get vector based on raw input
            newSpeed = newSpeed.normalized;                                         // normalise
            newSpeed = newSpeed * PlayerSpeed;                                      // multiply by player speed
            newSpeed = Quaternion.Euler(0, -45, 0) * newSpeed;                      // rotate by 45 degrees
            _myRigidBody.velocity = newSpeed;                                       // set rigid body velocity to new velocity

        }
        else
        {
            _inputTimer += Time.deltaTime;
        }


        // check for items in pickup zone
        if (_finishedShopping == false)
        {
            // Check if any items in our pickup zone - since it gets a list of all items in the game, we don't want to do it too often
            _checkPickupZoneTimer -= Time.deltaTime;
            if (_checkPickupZoneTimer <= 0.0f)
            {
                _checkPickupZoneTimer = 0.1f;
                // Check if anything is in our pickup zone
                ShoppingItem[] shoppngItems = GameObject.FindObjectsOfType<ShoppingItem>();

                _currentItemInZone = null;
                foreach (ShoppingItem item in shoppngItems)
                {
                    if (_myPickupZone.bounds.Contains(item.transform.position))
                    {
                        _currentItemInZone = item;
                        break;
                    }
                }
            }
        }
        else
        {
            _currentItemInZone = null;

            // check for checkout counter in our zone
            _checkoutInZone = false;
            GameObject[] counters = GameObject.FindGameObjectsWithTag("Checkout");
            foreach(GameObject counter in counters)
            {
                if (_myPickupZone.bounds.Contains(counter.transform.position))
                {
                    // checkout counter is in our zone!
                    _checkoutInZone = true;
                    _checkOutObject = counter;
                    break;
                }
            }
            
        }

 
        // check to pickup
        if (_pickupPressed == false)
        {
            if (Input.GetAxis("PickUp") > Mathf.Epsilon)
            {
                _pickupPressed = true;

                // check if we are picking an item up
                if (_currentItemInZone != null)
                {
                    Debug.Log("I picked up a " + _currentItemInZone.name);
                    _currentItemInZone.OnPickup();
                    _checkPickupZoneTimer = 0.0f;       // reset this timer so that we know in the next update already to disable the pickup indicator
                    _currentItemInZone = null;

                    playSound(PickupSound);

                    // check if we have finished collecting all of the items
                    checkItemListComplete();
                }

                // check if we are checking out
                if(_checkoutInZone)
                {
                    if (_checkingOut == false)
                    {
                        // start timer for checking out
                        Ui_CheckoutBar.gameObject.SetActive(true);
                        Ui_CheckoutBar.SetBarPercentage(0.0f);
                        _checkoutTimer = 0.0f;
                        _checkingOut = true;
                        _checkOutObject?.GetComponent<Checkout>().AnimateCheckout(true);

                    }
                }
            }
        }
        else
        {
            if (Input.GetAxis("PickUp") <= Mathf.Epsilon)
            {
                _pickupPressed = false;

                // no longer checking out
                _checkingOut = false;
                Ui_CheckoutBar.gameObject.SetActive(false);
                _checkOutObject?.GetComponent<Checkout>().AnimateCheckout(false);
            }
            else
            {
                // check if we are checking out
                if (_checkoutInZone)
                {
                    if (_checkingOut == true)
                    {
                        // update timer
                        _checkoutTimer += Time.deltaTime;
                        if(_checkoutTimer >= CheckoutTime)
                        {
                            //Debug.Log("I have won!!!");
                            Ui_CheckoutBar.gameObject.SetActive(false);
                            SetBackgroundMusicActive(false);
                            playSound(WinningSound, 0.2f, 1.0f);
                            _gameFinished = true;
                            GameManager.MainGameManager.Player_Won();
                        }
                        else
                        {
                            Ui_CheckoutBar.SetBarPercentage(_checkoutTimer / CheckoutTime);
                        }
                        _checkingOut = true;
                    }
                }
            }
        }

        // check for pause/exit
        bool escPressed = Input.GetButtonDown("Cancel");
        if(escPressed)
        {
            GameManager.MainGameManager.OnPause();
        }

        if (_currentItemInZone != null)
        {
            PickUpIndicator.SetActive(true);
        }
        else
        {
            PickUpIndicator.SetActive(false);
        }

        if (_finishedShopping == true)
        {
            if (_checkingOut == false)
            {
                if (_checkoutInZone == true)
                {
                    CounterIndicator.SetActive(false);
                    CheckoutIndicator.SetActive(true);
                }
                else
                {
                    CounterIndicator.SetActive(true);
                    CheckoutIndicator.SetActive(false);
                }
            }
            else
            {
                CounterIndicator.SetActive(false);
                CheckoutIndicator.SetActive(false);

            }
        }

        // Take Damage for any cough clouds
        handleDamage();

        // subtract time left
        setRemainingTime(_levelTimer - Time.deltaTime);

        // Handle timer flashing
        if (_timerFlashNo > 0)
        {
            _timerFlashTimer += Time.deltaTime;
            if (_timerFlashTimer > _timerFlashPeriod)
            {
                if (Ui_TimerText.gameObject.activeSelf == true)
                {
                    Ui_TimerText.gameObject.SetActive(false);
                    _timerFlashTimer = 0.0f;
                }
                else
                {
                    Ui_TimerText.gameObject.SetActive(true);
                    _timerFlashTimer = 0.0f;
                    _timerFlashNo--;
                }
            }
        }


    }

    private void checkItemListComplete()
    {
        ListItem[] listItems = GameObject.FindObjectsOfType<ListItem>();
        bool gotEverything = true;
        foreach(ListItem item in listItems)
        {
            if(item.IsChecked == false)
            {
                gotEverything = false;
                break;
            }
        }

        if(gotEverything)
        {
            Debug.Log("I have all the items I need!");
            _finishedShopping = true;
            CounterIndicator.SetActive(true);
        }
    }


    private void updatePersonBodyDir(float horizontalInput, float verticalInput)
    {
        float minSpeed = 0.5f;

        if ((horizontalInput > minSpeed) && (verticalInput > minSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthEast;
        }
        else if ((horizontalInput < -minSpeed) && (verticalInput > minSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthWest;
        }
        else if ((horizontalInput > minSpeed) && (verticalInput < -minSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthEast;
        }
        else if ((horizontalInput < -minSpeed) && (verticalInput < -minSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthWest;
        }
        else if (horizontalInput > minSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.East;
        }
        else if (horizontalInput < -minSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.West;
        }
        else if (verticalInput > minSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.North;
        }
        else if (verticalInput < -minSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.South;
        }

        if (_myRigidBody.velocity.magnitude > minSpeed)
        {
            _myPersonBody.IsWalking = true;
        }
        else
        {
            _myPersonBody.IsWalking = false;
        }
    }


    private void adjustGermAmount(float adjust)
    {
        _germLevel += adjust;

        // cannot go lower than 0
        if (_germLevel < 0)
        {
            _germLevel = 0;
        }

        // cannot go greater than max level
        if (_germLevel >= MaxGermLevel)
        {
            _germLevel = MaxGermLevel;

            // we have lost!
            onLosing();
            GameManager.MainGameManager.Player_Diseased();
        }

        Ui_GermoMeter.SetGermPercentage(_germLevel / MaxGermLevel);
    }


    private void handleDamage()
    {
        _coughTimer += Time.deltaTime;
        // take damage for every 10 milliseconds you are in the cough cloud
        if (_coughTimer >= 0.01f)
        {
            _coughTimer = 0.0f;
            _currentCaughs.RemoveAll(item => item == null);    // remove cough objects that were possibly destroyed
            foreach (Cough cough in _currentCaughs)
            {
                adjustGermAmount(cough.CoughDamageRate);
            }
        }
    }

    private void setRemainingTime(float remainingTime)
    {
        _levelTimer = remainingTime;
        if (_levelTimer < 0.0f)
        {
            _levelTimer = 0.0f;
            TimeOver();
        }

        int minutesLeft = (int)(_levelTimer / 60.0f);
        int secondsLeft = (int)(_levelTimer) - minutesLeft * 60;
        string timeString = string.Format("{0:D2}:{1:D2}", minutesLeft, secondsLeft);

        Ui_TimerText.text = timeString;

        if (_timerWarned == false)
        {
            if (_levelTimer < WarningTime)
            {
                Color newColor = new Color();
                newColor.a = 1;
                newColor.r = 1;
                newColor.g = 0;
                newColor.b = 0;
                Ui_TimerText.color = newColor;

                playSound(TimeLowSound);
                FlashTimer();

                _timerWarned = true;
            }
        }
    }

    private void FlashTimer()
    {
        _timerFlashNo = _timerFlashFlashTimes;
        _timerFlashTimer = _timerFlashPeriod;
    }


    private void TimeOver()
    {
        Debug.Log("TIME OVER!");
        onLosing();
        // show fail screen
        GameManager.MainGameManager.Player_TimeOver();
    }

    private Collider getCurrentPickUpZone()
    {
        Collider returnCollider = null;
        // get pickup zone collider
        foreach (Collider collider in GetComponentsInChildren<Collider>())
        {
            if (collider.gameObject.name == "PickupZone")
            {
                returnCollider = collider;
            }
        }
        if (returnCollider == null)
        {
            Debug.LogError("I don't have a PickUp zone collider!");
        }
        return returnCollider;
    }


    private void OnGermHit(GermParticle germ)
    {
        adjustGermAmount(2);
        playSound(SplatSound);
    }

    // plays audio clip, but only if the same clip is currently not playing, and if there is a free audio source
    private void playSound(AudioClip clip, float volume = 1.0f, float offset = 0.0f)
    {
        AudioSource emptySource = null;

        foreach (AudioSource audio in _audioScources)
        {
            if ((audio.isPlaying == false) && (emptySource == null))
            {
                emptySource = audio;
            }
            else if((audio.isPlaying == true) && (audio.clip == clip))
            {
                // audio clip is already playing, do nto start it again
                emptySource = null;
                break;
            }
        }

        if(emptySource != null)
        {
            emptySource.clip = clip;
            emptySource.volume = volume;
            emptySource.time = offset;
            emptySource.Play();
        }
    }


    // Public methods

    // To be called by germ detector
    public void CoughingZoneEnter(Cough cougher)
    {
        if (cougher != null)
        {
            _currentCaughs.Add(cougher);
            Debug.Log("I am being coughed on by " + cougher.name);
        }
    }

    // To be called by germ detector
    public void CoughingZoneExit(Cough cougher)
    {
        _currentCaughs.Remove(cougher);
    }


    // handle all common stuff for losing
    private void onLosing()
    {
        SetBackgroundMusicActive(false);
        playSound(LosingSound, 0.2f);
        _gameFinished = true;
    }

    private void SetBackgroundMusicActive(bool active)
    {
        AudioSource backgroundPlayer = Camera.main.gameObject.GetComponent<AudioSource>();
        if(active)
        {
            if(backgroundPlayer.isPlaying == false)
            {
                backgroundPlayer.Play();
            }
        }
        else
        {
            backgroundPlayer.Stop();
        }
        
    }

}
