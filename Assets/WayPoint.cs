﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WayPoint : MonoBehaviour
{

    /**** Public Properties ****/
    [Tooltip("Time that shopper person should wait at this waypoint")]
    public float PauseTime;

    [Tooltip("Direction the shopper person should have when stopping at this waypoint")]
    public PersonBody.Direction DirectionToFace;

    [Tooltip("Cough period that person should use (will use default if this is 0.0f)")]
    public float CoughPeriod = 0.0f;

    [Tooltip("If the person should cough immediately after reaching this waypoint")]
    public bool CoughImmediately = false;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
