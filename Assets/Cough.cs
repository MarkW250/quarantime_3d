﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cough : MonoBehaviour
{
    [Tooltip("Damage per 10ms")]
    public float CoughDamageRate = 1.0f;
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }


    // Called after cough circle has faded
    public void CoughFadeFinished()
    {
        Destroy(this.gameObject);
    }


}
