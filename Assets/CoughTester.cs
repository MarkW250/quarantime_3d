﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoughTester : MonoBehaviour
{
    /**** Public properties ****/
    public GameObject ParticlePrefab;

    /**** Private state variables ****/
    private float _spawnTimer = 0.0f;
    private bool _spawned = false;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (_spawned == false)
        {
            _spawnTimer += Time.deltaTime;
            if (_spawnTimer > 1)
            {
                GameObject newPArticle = GameObject.Instantiate(ParticlePrefab, new Vector3(0,0,0), new Quaternion());
                _spawned = true;
            }

        }
    }
}

