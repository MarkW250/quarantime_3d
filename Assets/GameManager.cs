﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    public static GameManager MainGameManager;

    /**** My componenets ****/
    private GameObject _myScreenblocker;
    private GameObject _myTutorialObjects;
    private int _tutorialObjectCount = 0;
    private int _tutorialObjectIndex = 0;
    private GameObject _myWinningObjects;
    private GameObject _myDiseasedObjects;
    private GameObject _myTimeOverObjects;
    private GameObject _myGameOverObjects;
    private GameObject _myPauseObjects;

    private bool _gameOver = false;

    /**** Private Types ****/
    private enum TutorialState
    {
        Init,
        WaitForInput,
        Finished
    };

    /**** Private state variables ****/
    private float _showTutorialTimer = 0.0f;
    private TutorialState _tutState = TutorialState.Init;
    private bool _gamePaused = false;
    private bool _gamePausePending = false;


    // Start is called before the first frame update
    void Start()
    {
        // set this to the main game manager
        if((MainGameManager != null) && (MainGameManager != this))
        {
            Destroy(this.gameObject);
            return;
        }

        MainGameManager = this;
        DontDestroyOnLoad(this.gameObject);

        SceneManager.sceneLoaded += this.OnSceneLoaded;

        Init();

    }

    private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        Start();
    }

    private void Init()
    {
        Canvas myCanvas = GetComponentInChildren<Canvas>();
        myCanvas.worldCamera = Camera.main;

        _gameOver = false;

        _myScreenblocker = getChildWithTag(transform, "ScreenBlocker");

        _myTutorialObjects = getChildWithTag(transform, "TutorialObjects");
        _tutorialObjectCount = _myTutorialObjects.transform.childCount;

        _myWinningObjects = getChildWithTag(transform, "WinningObjects");
        _myWinningObjects.SetActive(false);

        _myDiseasedObjects = getChildWithTag(transform, "DiseasedObjects");
        _myDiseasedObjects.SetActive(false);

        _myTimeOverObjects = getChildWithTag(transform, "TimeOverObjects");
        _myTimeOverObjects.SetActive(false);

        _myGameOverObjects = getChildWithTag(transform, "GameOverObjects");
        _myGameOverObjects.SetActive(false);

        _myPauseObjects = getChildWithTag(transform, "PauseObjects");
        _myPauseObjects.SetActive(false);

        stopTimeAndBlockScreen(false);
    }

    // Update is called once per frame
    void Update()
    {
        bool spacePressed = Input.GetButtonDown("PickUp");
        bool escPressed = Input.GetButtonDown("Cancel");

        if(_gamePaused)
        {
            if(escPressed)
            {
                Application.Quit();
            }
            else if(spacePressed && (_tutState == TutorialState.Finished))
            {
                _gamePausePending = false;
                stopTimeAndBlockScreen(false);
                _myPauseObjects.SetActive(false);
            }
        }

        if (_gamePausePending)
        {
            _gamePausePending = false;
            stopTimeAndBlockScreen(true);
            _myPauseObjects.SetActive(true);
        }

        // handle tutorial state machine
        handleTutorialState(spacePressed);

        if(_gameOver == true)
        {
            _myGameOverObjects.SetActive(true);

            if(escPressed)
            {
                Application.Quit();
            }
            if(spacePressed)
            {
                SceneManager.LoadScene(SceneManager.GetActiveScene().name);
            }
        }
    }

    private void handleTutorialState(bool spacePressed)
    {
        switch(_tutState)
        {
            case TutorialState.Init:
                {
                    _showTutorialTimer += Time.deltaTime;
                    if (_showTutorialTimer >= 0.1f)
                    {
                        // start showing tutorial
                        stopTimeAndBlockScreen(true);
                        _myTutorialObjects.transform.GetChild(0).gameObject.SetActive(true);

                        // disable all objects except the first
                        if (_tutorialObjectCount > 1)
                        {
                            _myTutorialObjects.transform.GetChild(1).gameObject.SetActive(true);
                        }
                        for(int i = 2; i < _tutorialObjectCount; i++)
                        {
                            _myTutorialObjects.transform.GetChild(i).gameObject.SetActive(false);
                        }
                        _tutorialObjectIndex = 1;

                        setTutState(TutorialState.WaitForInput);
                    }
                    break;
                }

            case TutorialState.WaitForInput:
                {
                    if (spacePressed)
                    {
                        // set current object to disabled
                        _myTutorialObjects.transform.GetChild(_tutorialObjectIndex).gameObject.SetActive(false);
                        _tutorialObjectIndex++;
                        if (_tutorialObjectIndex < _tutorialObjectCount)
                        {
                            // enable next tutorial object
                            _myTutorialObjects.transform.GetChild(_tutorialObjectIndex).gameObject.SetActive(true);
                        }
                        else
                        {
                            // otherwise, we have finished the tutorial
                            setTutState(TutorialState.Finished);
                        }
                    }
                    break;
                }
                

            default:
                break;
        }
    }

    private void setTutState(TutorialState newState)
    {
        switch(newState)
        {
            case TutorialState.Finished:
                {
                    // finished tutorial, resume game and make sure to disable all tutorial objects
                    for (int i = 0; i < _tutorialObjectCount; i++)
                    {
                        _myTutorialObjects.transform.GetChild(i).gameObject.SetActive(false);
                    }

                    stopTimeAndBlockScreen(false);
                    break;
                }

            default:
                break;
        }
        _tutState = newState;
    }


    private void stopTimeAndBlockScreen(bool pauseLevel)
    {
        if (pauseLevel)
        {
            _gamePaused = true;
            _myScreenblocker.SetActive(true);
            Time.timeScale = 0.0f;
        }
        else
        {
            _gamePaused = false;
            _myScreenblocker.SetActive(false);
            Time.timeScale = 1.0f;
        }
    }

    private GameObject getChildWithTag(Transform transformToCheck, string tag)
    {
        GameObject retObject = null;

        for(int i = 0; i < transformToCheck.childCount; i++)
        {
            Transform child = transformToCheck.GetChild(i);
            if (child.gameObject.tag == tag)
            {
                retObject = child.gameObject;
                break;
            }
            else
            {
                retObject = getChildWithTag(child, tag);
            }
        }

        return retObject;
    }


    /**** Public methods ****/
    // called when player time is over
    public void Player_TimeOver()
    {
        stopTimeAndBlockScreen(true);
        _gameOver = true;
        _myTimeOverObjects.SetActive(true);
    }

    // called when player is diseased
    public void Player_Diseased()
    {
        stopTimeAndBlockScreen(true);
        _gameOver = true;
        _myDiseasedObjects.SetActive(true);
    }

    // called when player has won
    public void Player_Won()
    {
        stopTimeAndBlockScreen(true);
        _gameOver = true;
        _myWinningObjects.SetActive(true);
    }

    // called when player presses pause
    public void OnPause()
    {
        _gamePausePending = true;
    }

    public bool IsPaused()
    {
        return _gamePaused;
    }
}
