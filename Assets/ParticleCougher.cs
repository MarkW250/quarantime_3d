﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleCougher : MonoBehaviour
{
    /**** Public properties ****/
    public GermParticle ParticlePrefab;

    public int NumberOfParticles = 2;

    /**** Private state variables ****/


    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < NumberOfParticles; i++)
        {
            float xRotation = transform.rotation.eulerAngles.x; //Random.Range(-60, 60) + transform.rotation.eulerAngles.x;
            float yRotation = Random.Range(-60, 60) + transform.rotation.eulerAngles.y;
            float zRotation = transform.rotation.eulerAngles.z;

            //GameObject newPArticle = GameObject.Instantiate(ParticlePrefab, transform.position, Quaternion.Euler(xRotation, yRotation, zRotation));
            GermParticle.SpawnGermParticle(ParticlePrefab, transform.position, Quaternion.Euler(xRotation, yRotation, zRotation));
        }

        GameObject.Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
}

