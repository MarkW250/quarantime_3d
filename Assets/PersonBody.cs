﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonBody : MonoBehaviour
{
    // **** Public types ****
    public delegate void OnGermHit(GermParticle germ);
    public event OnGermHit GermHitEvent;

    // Direction enumerator
    public enum Direction
    {
        None,
        North,
        NorthEast,
        East,
        SouthEast,
        South,
        SouthWest,
        West,
        NorthWest
    }

    // **** Public Properties ****
    public string DefaultBodyObjectName = "BodyEast";


    // **** Private types ****

    // struct for mapping direction to which gameobject should be enabled when going in that direction
    struct DirectionBodyMappingT
    {
        public DirectionBodyMappingT(Direction dir, string bodyName, float yRotation, bool flipX)
        {
            Direction = dir;
            BodyObjectName = bodyName;
            ColliderRotation_y = yRotation;
            GraphicsFlip_x = flipX;
        }

        public Direction Direction;
        public string BodyObjectName;
        public float ColliderRotation_y;
        public bool GraphicsFlip_x;
    }



    // **** Private Properties ****

    // Mapping table for direction to gameobject name that should be enabled
    private DirectionBodyMappingT[] _directionBodyMappings = new DirectionBodyMappingT[]
    {
        new DirectionBodyMappingT( Direction.East,          "BodyEast",         0.0f,    false ),
        new DirectionBodyMappingT( Direction.NorthEast,     "BodyNorthEast",  -45.0f,    false ),
        new DirectionBodyMappingT( Direction.North,         "BodyNorth",      -90.0f,    false ),
        new DirectionBodyMappingT( Direction.NorthWest,     "BodyNorthWest", -135.0f,    true  ),
        new DirectionBodyMappingT( Direction.West,          "BodyWest",      -180.0f,    true  ),
        new DirectionBodyMappingT( Direction.SouthWest,     "BodySouthWest", -225.0f,    false  ),
        new DirectionBodyMappingT( Direction.South,         "BodySouth",     -270.0f,    false ),
        new DirectionBodyMappingT( Direction.SouthEast,     "BodySouthEast", -315.0f,    true )
    };

    // **** My objects ****
    private GameObject _myColliders;

    // **** Private state variables ****
    private GameObject _currentlyActiveGameObject = null;
    private Direction _currentDirection;
    private bool _isWalking;
    private float _yRotation;



    // **** Public Methods ****
    public Direction CurrentDirection
    {
        get
        {
            return _currentDirection;
        }
        set
        {
            setNewDir(value);
        }
    }

    public bool IsWalking
    {
        get
        {
            return _isWalking;
        }
        set
        {
            _isWalking = value;
            handleAnimationState();
        }
    }


    // **** Private Methods - Unity Engine ****
    // Start is called before the first frame update
    void Start()
    {
        foreach(Transform trans in transform)
        {
            if(trans.tag == "Colliders")
            {
                _myColliders = trans.gameObject;
            }
        }
        if(_myColliders == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a colliders object!");
        }

        _currentlyActiveGameObject = transform.Find(DefaultBodyObjectName).gameObject;
    }

    // Update is called once per frame
    void Update()
    {

    }

    // Fixed update is called once per physics cycle
    private void FixedUpdate()
    {
        _myColliders.transform.localRotation = Quaternion.Euler(_myColliders.transform.rotation.eulerAngles.x, _yRotation, _myColliders.transform.rotation.eulerAngles.z);
    }


    // **** Private Methods ****
    private void setNewDir(Direction newDir)
    {
        if (_currentDirection != newDir)
        {
            bool flipX = false;
            float collidersRotation = 0.0f;

            _currentDirection = newDir;

            // de-activate current object
            if (_currentlyActiveGameObject != null)
            {
                _currentlyActiveGameObject.SetActive(false);
            }

            _currentlyActiveGameObject = null;
            // look for new object to set active
            foreach (DirectionBodyMappingT mapping in _directionBodyMappings)
            {
                if (mapping.Direction == _currentDirection)
                {
                    _currentlyActiveGameObject = transform.Find(mapping.BodyObjectName).gameObject;
                    flipX = mapping.GraphicsFlip_x;
                    collidersRotation = mapping.ColliderRotation_y;
                }
            }

            if (_currentlyActiveGameObject == null)
            {
                Debug.LogError("Could not find body object for direction " + newDir.ToString());
                _currentlyActiveGameObject = transform.Find(DefaultBodyObjectName).gameObject;
            }

            _currentlyActiveGameObject.SetActive(true);

            SpriteRenderer sprite = _currentlyActiveGameObject.GetComponentInChildren<SpriteRenderer>();
            sprite.flipX = flipX;
            _yRotation = collidersRotation;
        }
    }

    void handleAnimationState()
    {
        Animator myAnimator = GetComponentInChildren<Animator>();
        if (myAnimator != null)
        {
            myAnimator.SetBool("Walking", _isWalking);
        }
    }


    // Called when a cough particle hits us
    public void OnCoughParticleHit(GermParticle particle)
    {
        GermHitEvent?.Invoke(particle);
    }




}
