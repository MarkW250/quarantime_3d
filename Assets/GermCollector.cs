﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GermCollector : MonoBehaviour
{
    private PlayerController _myPlayer;

    // Start is called before the first frame update
    void Start()
    {
        _myPlayer = transform.root.GetComponent<PlayerController>();
    }

    // Update is called once per frame
    void Update()
    {

    }


    private void OnTriggerEnter(Collider collision)
    {
        if ((collision.gameObject.tag == "Cough"))
        {
            if (_myPlayer != null)
            {
                _myPlayer.CoughingZoneEnter(collision.gameObject.GetComponentInParent<Cough>());
            }
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (_myPlayer != null)
        { 
            if ((collision.gameObject.tag == "Cough"))
            {
                _myPlayer.CoughingZoneExit(collision.gameObject.GetComponentInParent<Cough>());
            }
        }
    }

}
