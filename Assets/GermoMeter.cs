﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GermoMeter : MonoBehaviour
{
    /**** Inspector Fields ****/
    [Tooltip("Color of filler when 'hot'")]
    [SerializeField]
    private Color BrightColor = new Color();

    [Tooltip("Color of filler when not 'hot'")]
    [SerializeField]
    private Color DarkColor = new Color();

    [Tooltip("Time to go from bright to dark color")]
    [SerializeField]
    private float CoolTime = 5.0f;

    /**** My Objects ****/
    private SpriteRenderer _myBarFiller;

    /**** Private state variables ****/
    private bool _inited = false;
    private float _hotTimer;

    // Start is called before the first frame update
    void Start()
    {
        Initialise();
    }

    // Update is called once per frame
    void Update()
    {
        if (_hotTimer > 0.0f)
        {
            _hotTimer -= Time.deltaTime;

            float percentageCooled = (CoolTime - _hotTimer) / (CoolTime);

            Color newColor = (BrightColor - percentageCooled * (BrightColor)) + (DarkColor * percentageCooled);
            _myBarFiller.color = newColor;
        }
        else
        {
            _myBarFiller.color = DarkColor;
        }
    }

    public void SetGermPercentage(float germPercentage)
    {
        if (_myBarFiller == null)
        {
            Initialise();
        }
        Vector2 newScale = new Vector2(germPercentage, _myBarFiller.transform.parent.localScale.y);
        _myBarFiller.transform.parent.localScale = newScale;
        _hotTimer = CoolTime;
    }

    private void Initialise()
    {
        if (_inited == false)
        {
            // Get my bar filler sprite componenet
            _myBarFiller = null;
            foreach (SpriteRenderer sprite in GetComponentsInChildren<SpriteRenderer>())
            {
                if (sprite.gameObject.name == "BarFiller")
                {
                    _myBarFiller = sprite;
                }
            }
            if (_myBarFiller == null)
            {
                Debug.LogError(gameObject.name + " does not have a bar filler sprite :(");
            }

            // reset hot timer
            _hotTimer = 0.0f;

            // set color to 'not hot'
            _myBarFiller.color = DarkColor;

            _inited = true;
        }
    }
}
