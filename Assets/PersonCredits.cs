﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PersonCredits : MonoBehaviour
{
    public bool Walking = false;

    private Animator _myAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _myAnimator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        _myAnimator.SetBool("Walking", Walking);
    }
}
