﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Checkout : MonoBehaviour
{
    private Animator _myAnimator;

    // Start is called before the first frame update
    void Start()
    {
        _myAnimator = transform.parent.gameObject.GetComponentInChildren<Animator>();
        if (_myAnimator == null)
        {
            Debug.LogError(gameObject.name + ": I don't have an Animator!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void AnimateCheckout(bool animate)
    {
         _myAnimator.SetBool("CheckingOut", animate);
    }



}
