﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GermParticle : MonoBehaviour
{

    static GermParticle[] GermPool = new GermParticle[200];

    public static void SpawnGermParticle(GermParticle particlePrefab, Vector3 spawnPosition, Quaternion spawnQuat)
    {
        GermParticle germParticleToUse = null;
        for(int i=0; (i<GermPool.Length) && (germParticleToUse==null); i++)
        {
            if(GermPool[i] == null)
            {
                GermPool[i] = GameObject.Instantiate(particlePrefab, spawnPosition, spawnQuat);
                germParticleToUse = GermPool[i];
            }
            else if(!GermPool[i].transform.gameObject.activeSelf)
            {
                germParticleToUse = GermPool[i];
            }
        }

        if(germParticleToUse != null)
        {
            germParticleToUse.Spawn(spawnPosition, spawnQuat);
        }
        else
        {
            Debug.LogWarning("Max available particles reached!");
        }
    }

    /**** My components ****/
    private Rigidbody _myRigidBody;

    /**** Private state variables ****/
    private float _timeToFade = 3;            // number of seconds before we fade away
    private float _fadeIncrements;            // how much fade should increment down
    private float _fadeIncTime = 0.1f;        // numbr of milliseconds between fading increments
    private float _fadeTimer = 0.0f;          // keep track of timer

    // Start is called before the first frame update
    void Start()
    {
        // Get my rigidy body 
        _myRigidBody = GetComponent<Rigidbody>();
        if(_myRigidBody == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a rigid body!");
        }
    }

    // Update is called once per frame
    void Update()
    {
        _fadeTimer += Time.deltaTime;
        if (_fadeTimer >= _fadeIncTime)
        {
            _timeToFade -= _fadeTimer;
            _fadeTimer = 0.0f;

            // set new alpha to materials
            MeshRenderer[] renderers = transform.root.GetComponentsInChildren<MeshRenderer>();
            foreach(MeshRenderer ren in renderers)
            {
                Color newColor = ren.material.color;
                if (_fadeIncrements > newColor.a)
                {
                    _fadeIncrements = newColor.a;
                }
                newColor.a = newColor.a - _fadeIncrements;
                ren.material.color = newColor;
            }

            // set alpha to sprites
            SpriteRenderer[] sprites = transform.root.GetComponentsInChildren<SpriteRenderer>();
            foreach (SpriteRenderer sprite in sprites)
            {
                Color newColor = sprite.material.color;
                if (_fadeIncrements > newColor.a)
                {
                    _fadeIncrements = newColor.a;
                }
                newColor.a = newColor.a - _fadeIncrements;
                sprite.material.color = newColor;
            }
        }

        if(_timeToFade <= 0.0f)
        {
            // Initiate self desctrucion!
            Destroy(this.gameObject);
        }

    }

    public void Spawn(Vector3 spawnPosition, Quaternion spawnQuat)
    {
        // Get my rigidy body 
        _myRigidBody = GetComponent<Rigidbody>();
        if (_myRigidBody == null)
        {
            Debug.LogError(gameObject.name + ": I don't have a rigid body!");
        }

        _timeToFade = 3;            // number of seconds before we fade away
         _fadeIncTime = 0.1f;        // numbr of milliseconds between fading increments
         _fadeTimer = 0.0f;          // keep track of timer

        _myRigidBody.transform.position = spawnPosition;
        _myRigidBody.transform.rotation = spawnQuat;

        // Start off with a force
        _myRigidBody.AddForce(transform.forward * Random.Range(5, 20));
        _myRigidBody.AddTorque(new Vector3(Random.Range(0, 1), 0, 0));

        // calculate fade increments
        _fadeIncrements = 1 / (_timeToFade / _fadeIncTime);

        gameObject.SetActive(true);
    }

    private void OnCollisionEnter(Collision collision)
    {
        PersonBody hitPerson = collision.collider.transform.root.GetComponentInChildren<PersonBody>();
        if (hitPerson != null)
        {
            // Inform the person body
            hitPerson.OnCoughParticleHit(this);

            // Initiate self desctrucion!
            gameObject.SetActive(false);
            //Destroy(this.gameObject);
        }
    }
}
