﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShopperPerson : MonoBehaviour
{
    // Public Properties
    [Tooltip("How many seconds between coughs for this person while shle is walking")]
    public float CoughPeriod = 1.0f;

    [Tooltip("Walking speed of this person")]
    public float WalkingSpeed = 2.5f;

    [Tooltip("Prefab to spawn for game object")]
    public GameObject CoughGameObject;

    [Tooltip("Parent game object of all waypoint objects that this shopper should follow")]
    public GameObject WayPointsCollection;

    [Tooltip("Is shopper should restart at first waypoint, or just stay at last waypoint")]
    public bool RecycleWayPoints = true;

    // My components
    Rigidbody _myRigidBody;
    AudioSource _myAudioSource;
    PersonBody _myPersonBody;
    WayPoint[] _myWayPoints;


    // Private state varaibles
    private float _coughTimer = 0.0f;
    private int _wayPointIndex = -1;
    private bool _atWayPoint = false;
    private float _wayPointTimer = 0.0f;
    private Vector3 _currentDestination;
    private float _currentCoughPeriod = 0.0f;


    // Start is called before the first frame update
    void Start()
    {
        _currentCoughPeriod = CoughPeriod;

        // get rigdiy body
        _myRigidBody = GetComponent<Rigidbody>();
        if (_myRigidBody == null)
        {
            Debug.LogError(gameObject.name + " does not have a rigid body 2d!");
        }

        // get audio source
        _myAudioSource = GetComponent<AudioSource>();
        if (_myAudioSource == null)
        {
            Debug.LogError(gameObject.name + " does not have an audio source!");
        }

        // get person body
        _myPersonBody = GetComponentInChildren<PersonBody>();
        if (_myPersonBody == null)
        {
            Debug.LogError("I don't have a PersonBody!");
        }

        // get waypoints
        _myWayPoints = WayPointsCollection.transform.GetComponentsInChildren<WayPoint>();
        if (_myWayPoints.Length > 0)
        {
            // start with first waypoint
            _wayPointIndex = 0;
            _currentDestination = _myWayPoints[_wayPointIndex].transform.position;
        }

    }

    // Update is called once per frame
    void Update()
    {

        // check if this guy should cough
        _coughTimer += Time.deltaTime;
        if ((_coughTimer >= _currentCoughPeriod) && (_currentCoughPeriod != 0))
        {
            _coughTimer = 0.0f;
            Cough();
        }


        // manage way finding
        if (_wayPointIndex >= 0)
        {
            if (_atWayPoint)
            {
                // wait for current way point timer to expire
                _wayPointTimer -= Time.deltaTime;
                if (_wayPointTimer < 0.0f)
                {
                    // once expired, set next destination to next waypoint
                    _wayPointIndex++;
                    if (_wayPointIndex >= _myWayPoints.Length)
                    {
                        // go back to first waypoint if the option is to do so
                        if (RecycleWayPoints == true)
                        {
                            _wayPointIndex = 0;
                        }
                        else
                        {
                            _wayPointIndex--;
                        }
                    }
                    _currentDestination = _myWayPoints[_wayPointIndex].transform.position;
                    _atWayPoint = false;

                    // reset cough period to normal setting
                    _currentCoughPeriod = CoughPeriod;
                }
            }
            else
            {
                Vector3 destinationVector = _currentDestination - transform.position;
                Vector3 newVelocity = new Vector3(0, 0, 0);
                // check if we have reached our destination
                if (destinationVector.magnitude < 0.5)
                {
                    _atWayPoint = true;

                    // set cough period to that specified by waypoint, unless it is 0, in which case use normal setting
                    if (_myWayPoints[_wayPointIndex].CoughPeriod > 0.0f)
                    {
                        _currentCoughPeriod = _myWayPoints[_wayPointIndex].CoughPeriod;
                    }
                    else
                    {
                        _currentCoughPeriod = CoughPeriod;
                    }

                    if (_myWayPoints[_wayPointIndex].CoughImmediately == true)
                    {
                        _coughTimer = _currentCoughPeriod - 0.02f;
                    }
                    else
                    {
                        _coughTimer = 0.0f;
                    }

                    _wayPointTimer = _myWayPoints[_wayPointIndex].PauseTime;
                    if (_myWayPoints[_wayPointIndex].DirectionToFace != PersonBody.Direction.None)
                    {
                        _myPersonBody.CurrentDirection = _myWayPoints[_wayPointIndex].DirectionToFace;
                    }
                }
                else
                {
                    // go towards current destination
                    Vector3 directionToDestination = destinationVector.normalized;
                    newVelocity = directionToDestination * WalkingSpeed;
                }

                _myRigidBody.velocity = newVelocity;
            }
        }

        // set person body direction according to veloctiy
        setPersonBodyDirection();

    }


    private void Cough()
    {
        GameObject coughObject = Instantiate(CoughGameObject, getCurrentCoughSource().transform.position, getCurrentCoughSource().transform.rotation);
        _myAudioSource.Play();
    }

    private GameObject getCurrentCoughSource()
    {
        GameObject returnObject = null;

        foreach (Transform childTrans in GetComponentsInChildren<Transform>())
        {
            if (childTrans.gameObject.name == "CoughSource")
            {
                returnObject = childTrans.gameObject;
            }
        }


        if (returnObject == null)
        {
            Debug.LogError("I don't have a cough source!");
        }
        return returnObject;
    }

    private void setPersonBodyDirection()
    {
        Vector3 currentVelocity = Quaternion.Euler(0, 45, 0) * _myRigidBody.velocity;
        float horizontalSpeed = currentVelocity.x;
        float verticalSpeed = currentVelocity.z;
        float minimumSpeed = 0.5f;

        if ((horizontalSpeed > minimumSpeed) && (verticalSpeed > minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthEast;
        }
        else if ((horizontalSpeed < -minimumSpeed) && (verticalSpeed > minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.NorthWest;
        }
        else if ((horizontalSpeed > minimumSpeed) && (verticalSpeed < -minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthEast;
        }
        else if ((horizontalSpeed < -minimumSpeed) && (verticalSpeed < -minimumSpeed))
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.SouthWest;
        }
        else if (horizontalSpeed > minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.East;
        }
        else if (horizontalSpeed < -minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.West;
        }
        else if (verticalSpeed > minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.North;
        }
        else if (verticalSpeed < -minimumSpeed)
        {
            _myPersonBody.CurrentDirection = PersonBody.Direction.South;
        }

        if ((Mathf.Abs(horizontalSpeed) > minimumSpeed) || (Mathf.Abs(verticalSpeed) > minimumSpeed))
        {
            _myPersonBody.IsWalking = true;
        }
        else
        {
            _myPersonBody.IsWalking = false;
        }
    }

}
