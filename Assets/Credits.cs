﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Credits : MonoBehaviour
{

    public bool ReadyToPlay = false;
    public GameObject disableOnLoading;
    public GameObject enableOnLoading;

    private bool loadNextScene = false;

    // Start is called before the first frame update
    void Start()
    {
        disableOnLoading.SetActive(true);
        enableOnLoading.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (loadNextScene)
        {
            SceneManager.LoadScene("Level_1");
        }


        bool spacePressed = Input.GetButtonDown("PickUp");
        if(ReadyToPlay && spacePressed)
        {
            disableOnLoading.SetActive(false);
            enableOnLoading.SetActive(true);
            loadNextScene = true;
        }

    }
}
