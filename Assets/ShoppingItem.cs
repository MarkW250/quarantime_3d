﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShoppingItem : MonoBehaviour
{
    /**** Public Properties ****/
    [Tooltip("List item that should be checked when I am picked up")]
    public ListItem ShoppingListItem;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /**** Public Methods ****/

    // To be called when someone is picking me up
    public void OnPickup()
    {
        if(ShoppingListItem != null)
        {
            ShoppingListItem.IsChecked = true;
        }
        // destroy myself
        Destroy(gameObject);
    }
}
