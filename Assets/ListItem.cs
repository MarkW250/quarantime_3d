﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ListItem : MonoBehaviour
{
    /**** Public Properties ****/
    [Tooltip("Sprite for item text not crossed")]
    public Sprite ItemText;

    [Tooltip("Sprite for item text crossed")]
    public Sprite ItemTextCrossed;

    /**** My Components ****/
    private SpriteRenderer _mySpriteRenderer;

    /**** Private state variables ****/
    private bool _isChecked = false;



    // Start is called before the first frame update
    void Start()
    {
        _mySpriteRenderer = GetComponent<SpriteRenderer>();
        IsChecked = false;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    /**** Public Methods ****/
    public bool IsChecked
    {
        get { return _isChecked; }
        set
        {
            _isChecked = value;
            if (_isChecked)
            {
                _mySpriteRenderer.sprite = ItemTextCrossed;
            }
            else
            {
                _mySpriteRenderer.sprite = ItemText;
            }
        }
    }
}
