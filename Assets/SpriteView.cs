﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteView : MonoBehaviour
{


    // Update is called once per change in scene
    [ExecuteInEditMode]
    void Update()
    {
        transform.LookAt(Camera.main.transform.position, -Vector3.up);
    }
}
